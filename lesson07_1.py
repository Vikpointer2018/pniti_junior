# https://youtu.be/27gRKiozCeI
# 1) Добавить вывод неподходящих запросов и отладки

from pprint import pprint
from itertools import product

queries = [
    'имя Ольга',
    'возраст 30',
    'младше 20',
    'старше 20',
    'живёт на Пушкина',
    'дом 10',
    'фамилия ростова',
    'зовут нташа'
]
ADDRESS_WORDS = {'дом','улица','живет','живёт','на','по','ул'}
NAME_WORDS = {'имя','зовут'}
AGE_WORDS = {'возраст','старше','младше'}
FAMILY_WORDS = {'фамилия'}

class Person:
    def __init__(self, name, age, addr):
        self.name, self.age, self.addr = name, age, addr
        self.key = (name, addr)
        
    def __repr__(self):
        return "Person('%s',%s,'%s')" \
               % (self.name, self.age, self.addr)

    def by_address(self,Q):
        Q = Q - ADDRESS_WORDS
        W = set(self.addr.split())
        rez = []
        for a, b in product(Q, W):
            rez += [(compare(a, b),a,b)]
        return max(rez)[0]

    def by_age(self,Q):
        q_age = max([int_val(q) for q in Q])
        if 'старше' in Q:
            return q_age < self.age
        if 'младше' in Q:
            return q_age > self.age
        return q_age + 5 >= self.age >= q_age - 5

    def by_name(self,Q):
        Q = Q - NAME_WORDS
        W = {self.name}
        rez = []
        for a, b in product(Q, W):
            rez += [(compare(a, b),a,b)]
        return max(rez)[0]

    def conforms(self, query):
        if type(query) == str:
            return self.fuzzy_compare(query)
        return False

        
    def fuzzy_compare(self, query):
        q = set(query.split())
        score = 0
        cnt = 0
        for m, f in zip(
            [ADDRESS_WORDS, NAME_WORDS, AGE_WORDS],
            [self.by_address, self.by_name, self.by_age]):
            if m & q:
                score += f(q)
                cnt += 1
        if cnt == 0:
            print('Некорректный запрос ',query)
        return score > 0.51



def int_val(obj):
    try:
        return abs(int(obj))
    except ValueError:
        return 0
        
def compare(s1, s2):
    s1, s2 = [ s.lower() for s in [s1, s2] ]
    ngrams = [ s1[i:i+3] for i in range(len(s1)) ]
    count = 0
    for ngram in ngrams:
        count += s2.count(ngram)
    return count/ max(len(s1), len(s2))
    
vik = Person("Виктор", 48, "Пушкина, 27, 2")
helen = Person("Ольга", 45, "Соликамский, 325, 12")
serg = Person("Олег", 35, "Фоминская, 69, 4")
nata1 = Person("Наташа", 15, "Кутаисская, 212, 14")
nata2 = Person("Наташа", 10, "Ростова, 21, 52")
nata3 = Person("нташа", 9, "Бродовский, 83, 1")

people = {
    vik.key: vik,
    helen.key: helen,
    serg.key: serg,
    nata1.key: nata1,
    nata2.key: nata2,
    nata3.key: nata3
}
"""
if __name__ == '__main__':
    out = []
    for a,b in [
        ('алгоритм','алгоритмы'),
        ('стол','столик'),
        ('стол','стул')
        ]:
        # print(a,b,compare(a,b))
        out += [(a,b,compare(a,b))]
    print(out)
    print([int_val(s) for s in ['c', '5']])
        
"""

for query, person in product(queries, people.values()):
    if person.conforms(query):
        pprint((query, person))
