# https://www.youtube.com/watch?v=aVzlnlC2XDY
# замена букв "x" на "y"
print("1)")
src = "asdxcvxcxvxvfgh hxxxfgxxyyha aaa xxxx"
dst = ""
for s in src:
    if s == 'x':
        dst += 'y'
    else:
        dst += s
print(src)
print(dst)

# Сосчитать произвеление чисел, кратных 3 и 5
print("2)")
mult = 1
for x in range(1, 100):
    if x%3==0 and x%5==0:
        mult *= x
print(mult)

# Заменить все буквы "x" на "y" без использования дополнительной
print("3)")
string = "asdxcvxcxvxvfgh hxxxfgxxyyha aaa xxxx"
string = string.replace('x','y')
print(string)
