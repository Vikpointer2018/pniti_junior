# https://youtu.be/N2bXg8BNNsc
# Исходный файл lesson01-2.py
#
# Добавлены пустые строки между логическими блоками кода
# Изменены названия переменных на более информативные
# В длинной строке выполнен перенос текста на следующую строку

print('1) замена букв "x" на "y"')
source_str = "asdxcvxcxvxvfgh hxxxfgxxyyha aaa xxxx"
result_str = ""
for s in source_str:
    if s == 'x':
        result_str += 'y'
    else:
        result_str += s
print(source_str)
print(result_str)


print('\n2) Сосчитать произвеление чисел, кратных 3 и 5')
mult = 1
for x in range(1, 100):
    if x%3==0 and x%5==0:
        mult *= x
print("Произведение чисел, кратных 3 и 5 \n"+\
      "на интервале от 1 до 99 включительно равно",mult)

 
print('\n3) Заменить все буквы "x" на "y" без использования дополнительной')
string = "asdxcvxcxvxvfgh hxxxfgxxyyha aaa xxxx"
print(string)
string = string.replace('x','y')
print(string)
