# https://www.youtube.com/watch?v=LI_WqZak_gA&t=38s
# 3)Сделать поиск нечётким
#   Наташа = наташа = нташа
#   120 = 121 = 118

from pprint import pprint
class Person:
    def __init__(self, name, age, addr, height):
        self.name, self.age, self.addr, self.height = name, age, addr, height
        self.key = (name, addr)
    def __repr__(self):
        return "Person('%s',%s,'%s',%s)" \
               % (self.name, self.age, self.addr, self.height)
    def approx_str(self, sample): # нечёткое сравнение двух строк
        smpl_list = list(sample[i:i+3].lower() for i in range(len(sample)))
        count = 0
        for ngram in smpl_list:
            count += self.name.lower().count(ngram)
        return count/max(len(sample), len(self.name))
    def approx_num(self, sample):
        return abs(self.height - sample)
    def compare_query(self, query):
        qu = query.split()
        if len(qu) < 2:
            return False
        if qu[0].lower()=='рост': # проверка роста
            if len(qu)==2: # рост 120 (равен нечётко +-2)
                return self.approx_num(int(qu[1])) < 3
            abc = 1 if qu[1]=='больше' or qu[1]=='выше' \
                  else -1 if qu[1]=='меньше' else 0
            # если рост не больше и не меньше - то равен нечётко(+-2)
            hgh = int(qu[2])
            return (abc == 0 and self.approx_num(hgh) < 3) \
                   or ((self.height - hgh)*abc > 0)
        elif qu[0].lower()=='имя' and len(qu)>1: # нечёткая проверка имени
            return self.approx_str(qu[1]) > 0.7
        else:
            print('Неизвестный запрос')
        return False
    
    
vik = Person("Виктор", 48, "Васильева, 27, 2", 188)
helen = Person("Елена", 45, "Соликамский, 325, 12", 165)
serg = Person("Сергей", 35, "Фоминская, 69, 4", 182)
nata1 = Person("Наташа", 15, "Кутаисская, 212, 14", 138)
nata2 = Person("Наташа", 10, "Комбайнёров, 21, 52", 120)
nata3 = Person("нташа", 9, "Бродовский, 83, 1", 118)

people = {
    vik.key: vik,
    helen.key: helen,
    serg.key: serg,
    nata1.key: nata1,
    nata2.key: nata2,
    nata3.key: nata3
}
print('В словаре содержатся следующие данные:\n',people)

print('\n3) Найти человека по имени Наташа ростом 120 см нечётким поиском:')
for person_key in people:
    if people[person_key].compare_query('имя Наташа') \
       and people[person_key].compare_query('рост 120'):
        print(people[person_key])



