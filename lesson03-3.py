# https://youtu.be/Hz69SupbI5M
# Типы данных
# 3) Сделать запрос данных от пользователя
company = {
    ("Виктор", "Мальцев"):{
        "b_year": 1973,
        "gender":"муж",
        "education":"3 класса церковно-приходской"},

    ("Игорь", "Кутявин"):{
        "b_year":1983,
        "gender":"муж",
        "education":"ПНИПУ ГНФ специалист" },

    ("Елена", "Балуева"):{
        "b_year":1979,
        "gender":"жен",
        "education":"ПГУ юрфак бакалавр" },

    ("Алексей", "Тимофеев"):{
        "b_year":1995,
        "gender":"муж",
        "education":"ВАТУ" },

    ("Светлана", "Алексеева"):{
        "b_year":2003,
        "gender":"жен",
        "education":"ПГПУ" },

    ("Михаил", "Боталов"):{
        "b_year":1970,
        "gender":"муж",
        "education":"ПНИПУ АКФ специалист" }}


sort_gender = input('Input gender (W-woman,M-man or empty for any)')
sort_asc = input('Input sort order (+ for find of the oldest persone)')

# проверка введённых параметров
if sort_gender.lower()=="w":
    sort_gender = 'woman'
elif sort_gender.lower() == "m":
    sort_gender = 'man'
else:
    sort_gender = 'person'

sort_asc = 'oldest' if sort_asc == '+' else 'youngest'

print('The ' + sort_asc + ' ' + sort_gender + ' will be find:')


older_man = {}
older_key = ()
for curr in company: # перебираем ключи сборного объекта
    curr_man = company.get(curr)
    if older_man == {}:
        older_man = curr_man # если это первый человек - то он пока самый-самый
        older_key = curr
        # с мальчиками/девочками сортировать возраст менее удобно
        # зададим фиксированное число в зависимости от направления сортировки
        older_man["b_year"] = 9999 if sort_asc == 'oldest' else -9999

    if curr_man.get("gender").startswith('м') and sort_gender == 'woman':
        continue # если пришёл мальчик, а ждали девочку - переходим к следующему
    if curr_man.get("gender").startswith('ж') and sort_gender == 'man':
        continue # если пришла девочка, а ждали мальчика - переходим к следующему

    if (curr_man.get("b_year") < older_man.get("b_year")) ^ (sort_asc == 'youngest'):
        older_man = curr_man # если очередной человек ещё лучше - перезаписываем его
        older_key = curr # и ключ тоже
        
print(older_key, older_man)
