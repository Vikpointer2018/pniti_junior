# https://www.youtube.com/watch?v=LI_WqZak_gA&t=38s
# 1)Описать классом свои данные

from pprint import pprint
class Person:
    def __init__(self, name, age, addr, height):
        self.name, self.age, self.addr, self.height = name, age, addr, height
        self.key = (name, addr)
    def __repr__(self):
        return "Person('%s',%s,'%s',%s)" % (self.name, self.age, self.addr, self.height)

vik = Person("Виктор", 48, "Васильева, 27, 2", 186)
helen = Person("Елена", 45, "Соликамский, 325, 12", 165)
serg = Person("Сергей", 35, "Фоминская, 69, 4", 182)
nata1 = Person("Наташа", 15, "Кутаисская, 212, 14", 138)
nata2 = Person("Наташа", 10, "Комбайнёров, 21, 52", 120)
nata3 = Person("нташа", 9, "Бродовский, 83, 1", 118)

people = {
    vik.key: vik,
    helen.key: helen,
    serg.key: serg,
    nata1.key: nata1,
    nata2.key: nata2,
    nata3.key: nata3
}
print('1) В словаре содержатся следующие данные:')
pprint(people)
