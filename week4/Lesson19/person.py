# https://youtu.be/wSL3WEM19us
# Доработка класса Человек
from datetime import date

from fio import Fio
from address import Address, TooManyPeopleException, MaleMaleException,\
    FemaleFemaleException


class Person:
    """
    Класс Человек, идентифицируется ключом из ФИО и года рождения
    должен уметь:
        -добавлять образование
        -возвращать текущий возраст
        -сравниваться приблизительно и по неполным данным
        -вселяться и выселяться из квартиры
    """
    def __init__(self, fio:Fio, b_year:int, addr:Address, edu:[]):
        self.fio = fio
        try:
            b_year = int(b_year)
            if b_year > 1900 and b_year < 2030:
                self.b_year = b_year
        except ValueError:
            self.b_year = 1970

        self.address = None
        self.education = [list(edu)] if edu!=None else []
        
        self.key = (fio, b_year)
        self.checkin(addr)

    def __repr__(self):
        return "Person:(%s, %s, %s, %s)" % (str(self.fio),\
               str(self.b_year),str(self.address),str(self.education))
            
    def add_education(self, new_educat):
        self.education += [list(new_educat)]
        
    def get_age(self): # 
        # Возвращает возраст, вычисляя его по текущей дате
        current_year = date.today().year
        age = current_year - self.b_year
        return age
    
    def fuzzy_compare(self, other):
        # нечёткое сравнение человека
        if other == None:
            return False
        try:
            result = abs(self.b_year - other.b_year)<2 \
                and self.fio.fuzzy_compare(other.fio)
        except KeyError:
            result = False
        return result
    
    def __eq__(self, other):
        # точное сравнение человека по ключу
        if other == None or self.__hash__() != other.__hash__():
            return False
        try:
            result = self.b_year == other.b_year and self.fio == other.fio
        except KeyError:
            result = False
        return result
    
    def __hash__(self):
        hash = self.fio.__hash__() + self.b_year*37
        return hash
    
    def __compare__(self, s1, s2):
        count = 0
        for ngram in (s1[i:i+3].lower() for i in range(len(s1))):
            count += s2.lower().count(ngram)
        return count / max(len(s1), len(s2)) > 0.5
    
    def checkin(self, addr:Address):
        # заселение по адресу
        # сначала выписаться из предыдущего адреса (запомнить его, даже None)
        old_addr = self.move_out()
        # затем попытаться прописаться по новому
        
        try:
            try:
                if addr.move_in(self.key):
                    self.address = addr
                    print(self.fio._name +" благополучно заселился в ", addr)
                    return True
            except Exception as e:
                # при неудаче перед выходом вселиться в старый адрес
                if old_addr != None:
                    old_addr.move_in(self.key)
                    self.address = old_addr
                print(self.fio._name +" не может заселиться в ", addr)
                raise e
        except TooManyPeopleException:
            print('Слишком много жильцов в квартире')
        except MaleMaleException:
            print('Двум мужчинам жить вместе не разрешается')
        except FemaleFemaleException:
            print('Двум женщинам жить вместе не разрешается')
        return False
        
    def move_out(self):
        # выселение из квартиры
        if self.address == None:
            return None
        old_addr = self.address
        old_addr.move_out(self.key)
        self.address = None
        return old_addr
        


if __name__ == "__main__":
    print('=== Создаём человека, мужчину ===')
    fio_oleg = Fio("Олег", "Владимирович", "Тараненко","м")
    addr_bolsh = Address("Большевистская", 165, 47)
    edu = ["ВШЭ",1993]
    oleg = Person(fio_oleg,1970, addr_bolsh, edu)
    print(oleg)
    print('\n=== Добавляем ему образование : ===')
    oleg.add_education(["Политех", 2000])
    print(oleg)
    print("\n=== Смотрим его возраст на сегодняшний день ===")
    print(oleg.get_age())
    print('\n==== Заселяем его на ул. Ленина ===')
    addr_len = Address("Ленина", 15, 28)
    check1 = oleg.checkin(addr_len)
    print(oleg)
    print("Test1", "OK" if check1 else "Failure")
    print('\n==== Смотрим жильцов на ул. Ленина ===')
    print(addr_len.people)
    
    nata = Person(Fio('Наталья', 'Александровна', 'Ростова', "ж"), 1978, \
                  Address("Уральская", 113, 2), ['Школа №1', 1995])
    alex = Person(Fio('Алексей', 'Михалыч', 'Кудряшов', "м"), 1968, \
                  Address("Васильева", 27, 5), ['ПВВКИУ', 1990]) 
    
    print('\n=== Подселяем к Олегу девочку ===')
    check2 = nata.checkin(addr_len)
    print(addr_len.people)
    print("Test2", "OK" if check2 else "Failure")
    
    print('\n=== Подселяем к ним мальчика ===')
    check3 = alex.checkin(addr_len)
    print(addr_len.people)
    print("Test3", "OK" if not check3 else "Failure")
    
    print('\n=== Переселяем Олега на Большвистскую ===')
    check4 = oleg.checkin(addr_bolsh)
    print("Test4", "OK" if check4 else "Failure")
    
    print('\n=== Подселяем к нему Алекса ===')
    check5 = alex.checkin(addr_bolsh)
    print(addr_len, ":", addr_len.people)
    print(addr_bolsh, ":", addr_bolsh.people)
    print("Test5", "OK" if not check5 else "Failure")

