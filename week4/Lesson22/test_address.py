import unittest
import address as ad
from fio import Fio

class TestAddressMethods(unittest.TestCase):
    
    addr_len = ad.Address("Ленина", 15, 28)
    addr_kompros = ad.Address("Комсомоьский пр.", "29А", "415")
    fio_oleg = Fio("Олег", "Владимирович", "Тараненко","м")
    fio_alex = Fio('Алексей', 'Михалыч', 'Кудряшов', "м",)
    fio_milen = Fio("милен","Васильевна","Макеева", "ж")
    oleg = (fio_oleg, 1970)
    alex = (fio_alex, 1968)
    milen = (fio_milen, 1985)
    nata = (Fio('Наталья', 'Александровна', 'Ростова', "ж"), 1978)
    
    def test_move_in(self):
        self.assertTrue(
            self.addr_len.move_in(self.milen), \
                "%s не удалось заселить в %s" % \
                    (self.milen[0], self.addr_len))
        self.assertRaises(
            ad.FemaleFemaleException, self.addr_len.move_in, self.nata)
        self.assertTrue(
            self.addr_len.move_in(self.oleg), \
                "%s не удалось заселить в %s" %\
                    (self.oleg[0], self.addr_len))
        self.assertRaises(
            ad.TooManyPeopleException, self.addr_len.move_in, self.alex)
        
    
    def test_move_out(self):
        self.assertTrue(
            self.addr_len.move_out(self.oleg),\
                "%s не удалось выселить из %s, хотя он там должен был жить" % \
                    (self.oleg[0], self.addr_len))
        self.assertFalse(
            self.addr_len.move_out(self.alex),\
                "%s удалось выселить из %s, хотя он там не жил" %\
                    (self.alex[0], self.addr_len))


        
if __name__ == '__main__':
    unittest.main()  
