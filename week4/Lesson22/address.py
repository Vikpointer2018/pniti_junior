class AddressException(Exception):
    pass

class TooManyPeopleException(AddressException):
    pass

class MaleMaleException(AddressException):
    pass

class FemaleFemaleException(AddressException):
    pass


class Address:
    
    def __init__(self, street, house, apart):
        self.street = street if street != None else ''
        self.house = house if house != None else ''
        self.apart = apart if apart != None else ''
        self.people = [] # храним только ключи людей
    
    def __repr__(self):
        return 'Address:("%s", %s-%s)' %\
            (self.street, self.house, self.apart)
            
    def move_in(self, person_key:tuple):
        # в одном адресе могут жить мужчина и/или женщина
        # но не двое мужчин и не две женщины
        if len(self.people) == 0:
            self.people.append(person_key)
            return True
        if len(self.people) >1:
            raise TooManyPeopleException("Too many people in the apartment")
        if self.people[0][0].gender == person_key[0].gender:
            if person_key[0].gender == "муж":
                raise MaleMaleException("Two men are not allowed")
            else:
                raise FemaleFemaleException("Two women are not alloved")
        else:
            self.people.append(person_key)
        return True
    
    def move_out(self, person_key:tuple):
        try:
            self.people.remove(person_key)
        except ValueError:
            print("Выселение невозможно: ", person_key[0],"не жил в квартире", self)
            return False
        return True
   