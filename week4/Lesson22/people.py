# https://www.youtube.com/watch?v=vMJfCxBsVoI
# Доработка класса человек
from pprint import pprint

from person import Person
from fio import Fio
from address import Address

class People(dict):
    """
    Класс Люди. Наследуется от словаря, содержит человеков
    Должен уметь:
        -добавлять в себя человека
        -возвращать человека по прямому совпадению, 
        -если такого не нашлось - по нечёткому совпадению 
           (в т.ч. по неполным данным, первого кто попадётся подходящий)
        -при отсутствии совпадения возвращать KeyError
    """
    def __compare__(self, s1:str, s2:str)->bool:
        count = 0
        if s1<s2:
            s1, s2 = s2, s1
        for ngram in (s1[i:i+3].lower() for i in range(len(s1))):
            count += s2.lower().count(ngram)
        com = count / max(len(s1), len(s2))
        print('people_compare: ',s1, s1, com)
        return com > 0.5
    
    def __getitem__(self, k1:tuple)->Person:# ключ tuple(fio, год)
        for k2, v in self.items():
            if k2[0] == k1[0]:
                if k2[1] == '' or k1[1] == '' or k2[1] == k1[1]:
                    return v
        
        for k2, v in self.items():
            if k2[0].fuzzy_compare(k1[0]):
                if k2[1] == '' or k1[1] == '' or abs(k2[1] - k1[1])<2:
                    return v
        raise KeyError("Человек категорически не нашёлся")
    
    def update(self, obj:Person):
        # сначала проверить на оответствие классу
        if obj == None or "<class 'person.Person'>" != str(type(obj)):
            raise TypeError("Trying update people by not Person object: ", str(type(obj)))
        # потом проверить на прямое соответствие
        for kk in self.keys():
            if obj.key == kk:
                # апдейтим содержимое кроме ФИО
                finded_person = self.__getitem__(kk)
                self.__sub_update(obj, finded_person)
                return True
        # если не нашелся ключ, проверить на нечёткое соответствие
        try:
            finded_person = self.__getitem__(obj.key)
            # и если такой человек уже есть в списке, ему обновляются данные
            # кроме ФИО и года
            self.__sub_update(obj, finded_person)
            
        except KeyError:
            # не нашёлся - добавляем
            super().update({obj.key:obj})
    
    def __sub_update(self, update_from, update_to):
        update_to.b_year = update_from.b_year
        update_to.address = update_from.address
        update_to.education = update_from.education
        
    def __repr__(self):
        return "People(%s)" % super().__repr__()
    

if __name__=="__main__":
    nata = Person(Fio('Наталья', 'Александровна', 'Ростова', "ж"), 1978, \
                  Address("Уральская", 113, 2), ['Школа №1', 1995])
    alex = Person(Fio('Алексей', 'Михалыч', 'Кудряшов', "м"), 1968, \
                  Address("Васильева", 27, 5), ['ПВВКИУ', 1990])  
    nata2 = Person(Fio('Наталья', 'Васильевна', 'Лунёва', "ж"), 1973, \
                  Address("Калинина", 36, 13), ['ПГУ', 1995])
    people = People()
    people.update(nata)
    people.update(alex)
    people.update(nata2)
    
    pprint(people)
    print("\n--- Поиск людей по неточным данным ---")
    print("\nПоиск: (Fio('алекс', 'михайлыч', 'кудряшЕв', 'м')', 1967) :\n", \
          people[(Fio('алекс', 'михайлыч', 'кудряшЕв', 'м'), 1967)])
    print("\nПоиск:(Fio('', '', 'ростова', ''),'')' :\n", \
          people[(Fio('', '', 'ростова', ''),'')])
    print("\nПоиск:(Fio('ната', '', '', ''),'')' :\n", \
          people[(Fio('ната', '', '', ''),'')])
    print('\n--- Добавляем образование Луневой Н. ---')
    nata2.add_education(['ПГТУ', 2000])
    print(people)
    
    print("\n--- Поиск однозначно отсутствующего человека ---")
    print("Поиск:(Fio('александр', '', '', ''),'')' :\n", \
          people[(Fio('александр', '', '', ''),'')])
    
            