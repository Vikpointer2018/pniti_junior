import unittest
from datetime import date

from person import Person
from address import Address,  TooManyPeopleException, FemaleFemaleException,\
    TooManyPeopleException
from fio import Fio

class TestPersonMethods(unittest.TestCase):
    
    addr_bolsh = Address("Большевистская", 165, 47)
    addr_len = Address("Ленина", 15, 28)
    addr_kompros = Address("Комсомольский пр.", "29А", "415")
    addr_vas = Address("Васильева", 27, 5)
    
    fio_oleg = Fio("Олег", "Владимирович", "Тараненко","м")
    fio_alex = Fio('Алексей', 'Михалыч', 'Кудряшов', "м",)
    fio_milen = Fio("милен","Васильевна","Макеева", "ж")
    
    edu = ["ВШЭ",1993]
    
    test_oleg_byear = 1970
    test_milen_byear = 1985
    
    oleg = Person(fio_oleg, test_oleg_byear, addr_bolsh, edu)

    alex = Person(
        fio_alex, 1968, addr_vas, ['ПВВКИУ', 1990])
    milen = Person(
        fio_milen, test_milen_byear, addr_len, ['Школа №1', 2003])
    nata = Person(
        Fio('Наталья', 'Александровна', 'Ростова', "ж"), 1978, addr_bolsh, [])
    helen = Person(
        Fio("Елена","Анатольевна","Макеева", "female"), 1983, None, [])
    helen2 = Person(
        Fio("лена","натолевна","макева", "ЖЕН"), 1984, addr_bolsh, [])
    helen3 = Person(
        Fio("Елена","Анатольевна","Макеева", "female"), 1983, addr_vas, [])
    alex2 = Person(
        Fio('алекс', 'михайлыч', 'кудряшЕв', "м"), 1968, \
            '', [])
    
    
    def test_get_age(self):
        current_year = date.today().year
        test_age_oleg = current_year - self.test_oleg_byear
        self.assertEqual(
            self.oleg.get_age(), test_age_oleg,\
                "Возраст %s не соответствует ожидаемому %s" % \
                    (self.oleg.get_age(), test_age_oleg))
        test_age_milen = current_year - self.test_milen_byear - 1
        self.assertNotEqual(
            self.milen.get_age(), test_age_milen,\
                "Возраст %s совпадает с %s" % \
                    (self.milen.get_age(), test_age_milen))


    def test_add_education(self):
        test_oleg_edu_before = len(self.oleg.education)
        self.assertEqual(
            test_oleg_edu_before, 1, "У Олега %s образований вместо одного" %\
                test_oleg_edu_before)
        self.oleg.add_education(["Политех", 2000])
        test_oleg_edu_after = len(self.oleg.education)
        self.assertEqual(
            test_oleg_edu_after, 2, "У Олега %s образований вместо двух" %\
                test_oleg_edu_after)
        
    
    def test_fuzzy_compare(self):
        self.assertTrue(self.helen.fuzzy_compare(self.helen2))
        self.assertFalse(self.helen.fuzzy_compare(self.milen))
        self.assertTrue(self.alex.fuzzy_compare(self.alex2))
    
    
    def test_eq(self):
        self.assertTrue(self.helen == self.helen3)
        self.assertFalse(self.helen == self.helen2)

 
    def test_checkin(self):
        self.assertTrue(
            self.milen.checkin(self.addr_len), \
                "%s не удалось заселить в %s" % \
                    (self.milen.fio, self.addr_len))
        self.assertFalse(
            self.nata.checkin(self.addr_len),\
                "%s удалось подселить к девочке в %s" % \
                    (self.nata.fio, self.addr_len))
        self.assertTrue(
            self.oleg.checkin(self.addr_len), \
                "%s не удалось заселить в %s" %\
                    (self.oleg.fio, self.addr_len))
        self.assertFalse(
            self.alex.checkin(self.addr_len),\
                "%s удалось подселить в полную комнату %s" % \
                    (self.alex.fio, self.addr_len))

    
    def test_move_out(self):
        old_addr_oleg = self.oleg.move_out()
        self.assertEqual(
            old_addr_oleg, self.addr_len,\
                "%s не совпадает с %s" % \
                    (old_addr_oleg, self.addr_len))
        old_addr_oleg = self.oleg.move_out()
        self.assertNotEqual(
            old_addr_oleg, self.addr_len,\
                "%s совпадает с %s" % \
                    (old_addr_oleg, self.addr_len))

        
if __name__ == '__main__':
    unittest.main()  

