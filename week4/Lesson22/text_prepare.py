import re
import pickle

file_war_src = open('war.txt', 'r', encoding='utf-8')
text = ''.join(file_war_src.readlines())
file_war_src.close()

text = re.sub('[^А-Яа-яёЁ.!?]', " ", text).strip()
text = re.sub('[.!?]\s+[А-Яа-яёЁ]', ' ', text)
text = re.sub('[.!?]', ' ', text)
text = re.sub('\s+',' ', text)

all_words = text.split()

words = list(set( w for w in all_words if len(w)>3 and w[0].isupper() ))
words.sort()


print(words)
pickle.dump(
    obj=words,
    file=open('names', 'wb')
)