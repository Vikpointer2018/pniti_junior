from pickle import load, dump
from itertools import product
from levenstein import distance


def compare(s1, s2):
    count = 0
    if s1<s2:
        s1, s2 = s2, s1
    for ngram in (s1[i:i+3].lower() for i in range(max(len(s1)-1, 2))):
        count += s2.lower().count(ngram)
    comp = min (1.0, count / max(len(s1)-2, len(s2)-2), 1)
    return comp


words = load(open('names', 'rb'))
trace_count = len(words) ** 2 // 20 # count iterates for 5% of all work
out = []
i = 0
for a, b in product(words, words):
    x1 = 1 if a == b else 0
    x2 = distance(a, b)
    x3 = compare(a, b)
    out.append([a, b, x1, x2, x3])
    if not i % trace_count:
        print(str(i//trace_count * 5) + "% ",a, b, x1, x2, x3)
    i += 1
dump(obj=out, file=open('out', 'wb'))