class Fio:
    """
    Класс Фамилия, Имя, Отчество, пол.
    Умеет сравниваться нечётко
    """
    def __init__(self, a_name, patron, surname, gender:str="m"):
        a_name = "" if a_name == None else a_name
        surname = "" if surname == None else surname
        if a_name == "" and surname == "":
            raise Exception('Fio is empty')
            return
        self._name = a_name
        self.surname = surname
        self.patron = "" if patron == None else patron
        self.gender = "" if gender == None or gender == '' \
            else "жен" if gender.lower().startswith("ж")\
                or gender.lower().startswith("f") else "муж"
        
    def __repr__(self):
        return 'Fio:(%s %s %s %s)' %\
            (self._name, self.patron, self.surname, self.gender)
    
    def fuzzy_compare(self, other):
        """
        Нечёткое сравнение ФИО.
        пол если указан, должен совпадать полностью,
        отчество опционально, фамилия и/или имя должны быть указаны
        """
        if other == None:
            return False
        try:
            if other.gender != '' and self.gender != other.gender \
                or other._name == '' and other.surname == ''\
                or other._name != '' and not self.__compare__(self._name, other._name)\
                or other.patron != '' and not self.__compare__(self.patron, other.patron)\
                or other.surname != '' and not self.__compare__(self.surname, other.surname):
                return False
        except (ValueError, KeyError):
            return False
        return True
    
    def __eq__(self, other):
        if other == None or self.__hash__() != other.__hash__():
            return False
        try:
            if self.gender != other.gender\
                or self._name != other._name\
                or self.patron != other.patron\
                or self.surname != other.surname:
                return False
        except KeyError:
            return False
        return True
    
    def __compare__(self, s1, s2):
        count = 0
        if s1<s2:
            s1, s2 = s2, s1
        for ngram in (s1[i:i+3].lower() for i in range(max(len(s1)-1, 2))):
            count += s2.lower().count(ngram)
        comp = count / max(len(s1), len(s2))
        print(s1, s2, comp)
        return comp >= 0.5
    
    def __hash__(self):
        hash = self._name.__hash__() + self.patron.__hash__() \
            + self.surname.__hash__() + self.gender.__hash__()
        return hash
    
    