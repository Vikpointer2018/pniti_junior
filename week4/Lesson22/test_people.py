import unittest
from people import People
from address import Address
from fio import Fio
from person import Person

class TestPeopleMethods(unittest.TestCase):
    
    addr_bolsh = Address("Большевистская", 165, 47)
    addr_len = Address("Ленина", 15, 28)
    addr_kompros = Address("Комсомольский пр.", "29А", "415")
    addr_vas = Address("Васильева", 27, 5)
    
    fio_oleg = Fio("Олег", "Владимирович", "Тараненко","м")
    fio_alex = Fio('Алексей', 'Михалыч', 'Кудряшов', "м",)
    fio_milen = Fio("милен","Васильевна","Макеева", "ж")
    
    test_nata_byear = 1978
    
    nata = Person(
        Fio('Наталья', 'Александровна', 'Ростова', "ж"), test_nata_byear, addr_bolsh, [])
    helen = Person(
        Fio("Елена","Анатольевна","Макеева", "female"), 1983, None, [])
    helen2 = Person(
        Fio("лена","натолевна","макева", "ЖЕН"), 1984, addr_bolsh, [])
    
    
    def test_update(self):
        people = People()
        count_before = len(people)
        self.assertEqual(count_before, 0, "Список людей изначально не пустой")
        people.update(self.helen)
        count_after = len(people)
        self.assertEqual(count_after, 1, "В списке людей не один человек")
        people.update(self.helen2)
        count_after = len(people)
        self.assertEqual(count_after, 1, "В списке людей не один человек")
        people.update(self.nata)
        count_after = len(people)
        self.assertEqual(count_after, 2, "В списке людей не два человека")
    
    def test_getitem(self):
        people = People()
        people.update(self.helen)
        people.update(self.nata)
        nat = people[Fio('наталья','','','female'),'']
        self.assertEqual(
            nat.b_year, self.test_nata_byear, "человек нашёлся неверно")
        alex_key = Fio('Александр','','','м'), ''
        self.assertRaises(
            KeyError, people.__getitem__, alex_key)
    
if __name__ == '__main__':
    unittest.main()