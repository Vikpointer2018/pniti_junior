# https://youtu.be/wSL3WEM19us
# Доработка класса Человек
from datetime import date

from fio import Fio
import address as adr


class Person:
    """
    Класс Человек, идентифицируется ключом из ФИО и года рождения
    должен уметь:
        -добавлять образование
        -возвращать текущий возраст
        -сравниваться приблизительно и по неполным данным
        -вселяться и выселяться из квартиры
    """
    def __init__(self, fio:Fio, b_year:int, addr:adr.Address, edu:[]):
        self.fio = fio
        try:
            b_year = int(b_year)
            if b_year > 1900 and b_year < 2030:
                self.b_year = b_year
        except ValueError:
            self.b_year = 1970

        self.address = None
        self.education = [list(edu)] if edu!=None else []
        
        self.key = (fio, b_year)
        self.checkin(addr)


    def __repr__(self):
        return "Person:(%s, %s, %s, %s)" % (str(self.fio),\
               str(self.b_year),str(self.address),str(self.education))

            
    def add_education(self, new_educat):
        self.education += [list(new_educat)]

        
    def get_age(self): # 
        # Возвращает возраст, вычисляя его по текущей дате
        current_year = date.today().year
        age = current_year - self.b_year
        return age

    
    def get_gender(self):
        return self.fio.gender

        
    def fuzzy_compare(self, other):
        # нечёткое сравнение человека
        if other == None:
            return False
        try:
            result = abs(self.b_year - other.b_year)<2 \
                and self.fio.fuzzy_compare(other.fio)
        except KeyError:
            result = False
        return result

    
    def __eq__(self, other):
        # точное сравнение человека по ключу
        if other == None or self.__hash__() != other.__hash__():
            return False
        try:
            result = self.b_year == other.b_year and self.fio == other.fio
        except KeyError:
            result = False
        return result

    
    def __hash__(self):
        hash = self.fio.__hash__() + self.b_year*37
        return hash
    
    
    def checkin(self, addr:adr.Address):
        # заселение по адресу
        # сначала выписаться из предыдущего адреса (запомнить его, даже None)
        old_addr = self.move_out()
        # затем попытаться прописаться по новому
        
        try:
            try:
                if addr == None or addr == '':
                    self.address = None
                    print(self.fio._name +" - бомж")
                    return True
                if addr.move_in(self.key):
                    self.address = addr
                    print(self.fio._name +" благополучно заселился в ", addr)
                    return True
            except Exception as e:
                # при неудаче перед выходом вселиться в старый адрес
                if old_addr != None:
                    old_addr.move_in(self.key)
                    self.address = old_addr
                print(self.fio._name +" не может заселиться в ", addr)
                raise e
        except adr.TooManyPeopleException:
            print('Слишком много жильцов в квартире')
        except adr.MaleMaleException:
            print('Двум мужчинам жить вместе не разрешается')
        except adr.FemaleFemaleException:
            print('Двум женщинам жить вместе не разрешается')
        return False

        
    def move_out(self):
        # выселение из квартиры
        if self.address == None or self.address =='':
            return None
        old_addr = self.address
        old_addr.move_out(self.key)
        self.address = None
        return old_addr
        
