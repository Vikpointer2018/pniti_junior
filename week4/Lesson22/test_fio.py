import unittest
from fio import Fio

class TestFioMethods(unittest.TestCase):
    
    helen = Fio("Елена","Анатольевна","Макеева", "female")
    helen2 = Fio("лена","натолевна","макева", "ЖЕН")
    helen3 = Fio("Елена","Анатольевна","Макеева", "female")
    milen = Fio("милен","Васильевна","Макеева", "ж")
    alex = Fio('Алексей', 'Михалыч', 'Кудряшов', "м")
    alex2 = Fio('алекс', 'михайлыч', 'кудряшЕв', "м")
    
    def test_fuzzy_compare(self):
        self.assertTrue(self.helen.fuzzy_compare(self.helen2))
        self.assertFalse(self.helen.fuzzy_compare(self.milen))
        self.assertTrue(self.alex.fuzzy_compare(self.alex2))
    
    def test_eq(self):
        self.assertTrue(self.helen == self.helen3)
        self.assertFalse(self.helen == self.helen2)


        
if __name__ == '__main__':
    unittest.main()       
