from itertools import product
from pickle import load
from levenstein import distance
from pprint import pprint

def compare(s1, s2):
    count = 0
    if s1<s2:
        s1, s2 = s2, s1
    for ngram in (s1[i:i+3].lower() for i in range(max(len(s1)-1, 2))):
        count += s2.lower().count(ngram)
    comp =count / max(len(s1)-2, len(s2)-2, 1, count)
    return comp

def is_equal(a_method, a, b):
    return a_method(a, b) > 0.6

words = load(open('names', 'rb'))
by_dist = []
by_comp = []
trace_count = len(words) ** 2 // 20 # count iterates for 5% of all work
i = 0
for a, b in product(words, words):
    if is_equal(distance, a, b):
        by_dist.append([a,b])
    if is_equal(compare, a, b):
        by_comp.append([a,b])
    
    if i%trace_count == 0: # print of intermediate data on working
        print(str(i//trace_count*5)+"% ", a, b, distance(a,b), compare(a, b))
    i += 1

print("\n\ncount dist = ", len(by_dist))   
print("count comp = ", len(by_comp))
pprint(by_dist, open('dist_war', 'w'))
pprint(by_comp, open('comp_war', 'w'))
