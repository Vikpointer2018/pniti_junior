import matplotlib.pyplot as plt
from pickle import load

rez = load(open('out','rb'))

to_plot = sorted(rez, key=lambda x : -x[3])
print(len(rez))

plt.plot(range(len(rez)), [ x[4] for x in to_plot], "r--")
plt.plot(range(len(rez)), [ x[3] for x in to_plot], "g--")

plt.show()
