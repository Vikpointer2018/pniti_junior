# https://youtu.be/yYmHANs3MNA
import numpy as np

# 1) Посчитать сумму ряда 0-100
x = np.array(range(101))
print(x)
print(np.mean(x))
print(np.sum(x))

# 2) Посчитать сумму ряда 0 - input()
n = int(input('Enter last element (n>0)'))
if n < 1:
    print('n incorrect, will be changed to 100')
    n = 100
y = np.array(range(n+1))
print(y)
print('Summ of sequence = ',np.sum(y))

# 3) Среднее среди 100 случайных чисел
z = np.mean(np.random.randint(0,101,100))
print('Average of 100 random numbers [0-100]',z)
