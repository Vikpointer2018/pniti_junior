# https://youtu.be/4m_P-3xW0lA
# 1) Оформить функциями поиск в списке

def find_person(list_persons, find_youngest=False):
    if list_persons == None or len(list_persons)==0:
        return []
    # По третьему заданию год рождения хранится в элементе 2
    older_man = list_persons[0]
    for curr_man in list_persons:
        if (curr_man[2] < older_man[2])^find_youngest:
            older_man = curr_man
    return older_man

people1 = [ "Виктор", "Мальцев", 1973, "муж", "3 класса церковно-приходской" ]
people2 = [ "Игорь", "Кутявин", 1983, "муж", "ПНИПУ ГНФ специалист" ]
people3 = [ "Елена", "Балуева", 1979, "жен", "ПГУ юрфак бакалавр" ]
people4 = [ "Алексей", "Тимофеев", 1995, "муж", "ВАТУ" ]
people5 = [ "Светлана", "Алексеева", 2003, "жен", "ПГПУ" ]
people6 = [ "Михаил", "Боталов", 1970, "муж", "ПНИПУ АКФ специалист" ]

people_list = [ people1, people2, people3, people4, people5, people6 ]

print('Oldest person is ', find_person(people_list))
print('Youngest person is ', find_person(people_list, True))
