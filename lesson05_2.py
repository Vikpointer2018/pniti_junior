# https://youtu.be/4m_P-3xW0lA
# 2) Оформить функциями поиск в словаре

def find_person(dict_persons, find_youngest=False, by_gender='person'):
    if dict_persons == None or len(dict_persons)==0:
        return {}
    older_man = {}
    older_key = ()
    better = -9999
    for curr in company: # перебираем ключи сборного объекта
        curr_man = company.get(curr)
        if older_man == {}:
            older_man = curr_man # если это первый человек - то он пока лучший
            older_key = curr
            better = -9999 if find_youngest else 9999

        if curr_man.get("gender").startswith("ж") and by_gender.lower().startswith("m"):
            continue
        if curr_man.get("gender").startswith("м") and by_gender.lower().startswith("w"):
            continue
    
        if  (curr_man["b_year"] < better)^find_youngest:
            older_man = curr_man # если очередной человек ещё старше - перезаписываем
            older_key = curr
            better = curr_man["b_year"]
    return {older_key : older_man}


company = {
    ("Виктор", "Мальцев"):{
        "b_year": 1973,
        "gender":"муж",
        "education":"3 класса церковно-приходской"},

    ("Игорь", "Кутявин"):{
        "b_year":1983,
        "gender":"муж",
        "education":"ПНИПУ ГНФ специалист" },

    ("Елена", "Балуева"):{
        "b_year":1979,
        "gender":"жен",
        "education":"ПГУ юрфак бакалавр" },

    ("Алексей", "Тимофеев"):{
        "b_year":1995,
        "gender":"муж",
        "education":"ВАТУ" },

    ("Светлана", "Алексеева"):{
        "b_year":2003,
        "gender":"жен",
        "education":"ПГПУ" },

    ("Михаил", "Боталов"):{
        "b_year":1970,
        "gender":"муж",
        "education":"ПНИПУ АКФ специалист" }}

print('Oldest person is ', find_person(company))
print('Youngest person is ', find_person(company, True))
print('Youngest man is ', find_person(company, True, 'm'))
print('Oldest woman is ', find_person(company, False,'w'))
