# https://youtu.be/27gRKiozCeI
# 3) Сделать аналогично для ФИО и адреса, разбитых на поля

from pprint import pprint
from itertools import product

queries = [
    'имя Ольга',
    'возраст 30',
    'младше 20',
    'старше 20',
    'живёт на Пушкина',
    'дом 10',
    'фамилия Ростова',
    'зовут нташа'
]

NAME_WORDS = {'имя','зовут'}
SURNAME_WORDS = {'фамилия'}
PATRON_WORDS = {'отчество'}
AGE_WORDS = {'возраст','старше','младше'}
STREET_WORDS = {'улица','живет','живёт','на','по','ул'}
HOUSE_WORDS = {'дом','д.'}
APART_WORDS = {'квартира', 'кв.'}

class Person:
    def __init__(self, name, patron, surname, age, street, house, apart):
        self.name, self.patron, self.surname, self.age,\
                   self.street, self.house, self.apart \
                   = name, patron, surname, age, street, house, apart
        self.key = (name, surname, patron, street, house, apart)
        
    def __repr__(self):
        return "Person('%s','%s','%s',%s,'%s','%s','%s')" \
               % (self.name, self.patron, self.surname, self.age,\
                  self.street, self.house, self.apart)


    def by_part(self, Q, part):
        if part == 'name':
            Q = Q - NAME_WORDS
            W = {self.name}
        elif part == 'surname':
            Q = Q - SURNAME_WORDS
            W = {self.surname}
        elif part == 'patron':
            Q = Q - PATRON_WORDS
            W = {self.patron}
        elif part == 'street':
            Q = Q - STREET_WORDS
            W = {self.street}
        elif part == 'house':
            Q = Q - HOUSE_WORDS
            W = {self.house}
        elif part == 'apart':
            Q = Q - APART_WORDS
            W = {self.apart}
        else:
            return 0
        rez = []
        for a, b in product(Q, W):
            rez += [(compare(a, b),a,b)]
        return max(rez)[0]

    
    def by_name(self,Q):
        return self.by_part(Q, 'name')

    def by_surname(self,Q):
        return self.by_part(Q, 'surname')

    def by_patron(self,Q):
        return self.by_part(Q, 'patron')

    def by_street(self,Q):
        return self.by_part(Q, 'street')

    def by_house(self,Q):
        return self.by_part(Q, 'house')

    def by_apart(self,Q):
        return self.by_part(Q, 'apart')


    def by_age(self,Q):
        q_age = max([int_val(q) for q in Q])
        if 'старше' in Q:
            return q_age < self.age
        if 'младше' in Q:
            return q_age > self.age
        return q_age + 5 >= self.age >= q_age - 5


    def conforms(self, query):
        if type(query) == str:
            return self.fuzzy_compare(query)
        return False

        
    def fuzzy_compare(self, query):
        q = set(query.split())
        score = 0
        cnt = 0
        for m, f in zip(
            [NAME_WORDS, SURNAME_WORDS, PATRON_WORDS, AGE_WORDS, \
             STREET_WORDS, HOUSE_WORDS, APART_WORDS ],
            [self.by_name, self.by_surname, self.by_patron, self.by_age, \
             self.by_street, self.by_house, self.by_apart]):
            if m & q:
                score += f(q)
                cnt += 1
        if cnt == 0:
            print('Некорректный запрос: ',query)
        return score > 0.51



def int_val(obj):
    try:
        return abs(int(obj))
    except ValueError:
        return 0
        
def compare(s1, s2):
    s1, s2 = [ s.lower() for s in [s1, s2] ]
    ngrams = [ s1[i:i+3] for i in range(len(s1)) ]
    count = 0
    for ngram in ngrams:
        count += s2.count(ngram)
    return count/ max(len(s1), len(s2))

def levenstein(a,b):
    n, m = len(a), len(b)
    if n > m:
        a, b = b, a
        n, m = m, n

    curr_row = range(n + 1) 
    for i in range(1, m + 1):
        prev_row, curr_row = curr_row, [i] + [0] * n
        for j in range(1, n + 1):
            add, delete, change = prev_row[j] + 1, curr_row[j - 1] + 1, prev_row[j - 1]
            if a[j - 1] != b[i - 1]:
                change += 1
            curr_row[j] = min(add, delete, change)

    return (m - curr_row[n])/m
    
    
vik = Person("Виктор","Фёдорович","Пашкевич", 48, "Пушкина", "27", "2")
helen = Person("Ольга","Борисовна","Достоевская", 45, "Соликамский", "325а", "12")
serg = Person("Олег","Михайлович","Сыроежкин", 35, "Фоминская", "69", "4")
nata1 = Person("Наташа","Алексеевна","Ростова", 15, "Кутаисская", "212", "14")
nata2 = Person("Наташа","Васильевна","Дворкина", 10, "Ростова", "21", "52/2")
nata3 = Person("нташа","сергевна","петросян", 9, "Бродовский", "83", "1")

people = {
    vik.key: vik,
    helen.key: helen,
    serg.key: serg,
    nata1.key: nata1,
    nata2.key: nata2,
    nata3.key: nata3
}

"""if __name__ == '__main__':
    out = []
    for a,b in [
        ('алгоритм','алгоритмы'),
        ('стол','столик'),
        ('стол','стул')
        ]:
        print(a,b,compare(a,b), levenstein(a,b))
        out += [(a,b,compare(a,b))]
    print(out)
    print([int_val(s) for s in ['c', '5']])
"""        


for query, person in product(queries, people.values()):
    if person.conforms(query):
        pprint((query, person))
