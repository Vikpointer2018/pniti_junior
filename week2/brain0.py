# Brain
cheat = ""

def was_asked(memory, q):
    if len(memory) == 0:
        return 0
    global cheat
    for question, answer in memory:
        if question == q:
            cheat = answer
            return 1
    return 0

def old_answer():
    global cheat
    res , cheat = cheat, ''
    return res

if __name__ == "__main__":
    memory = [
        ('Ты умеешь говорить','да'),
        ('Ты умеешь готовить','нет'),
        ]
    print(was_asked(memory, 'Ты умеешь готовить'))
    print(old_answer())
    print(old_answer())
