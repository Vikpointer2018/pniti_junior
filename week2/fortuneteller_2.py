# https://youtu.be/OUo0DNRqznc
# Гадалка

from brain2 import Brain

brain = Brain()
prompt = "Что Вас интересует?"
question = "1"

while True:
    print(prompt, end = ' ')
    question = input()
    if question == "":
        print('Приходите ещё!')
        brain.save()
        break
    answer = brain.think(question)
    print(answer)
