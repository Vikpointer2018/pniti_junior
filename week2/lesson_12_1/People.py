# https://youtu.be/gyc6CPgxdG4
# ООП
# 1) Описать себя как объект
from Fio import Fio
from Gender import Gender

class People:
    fio = None
    birth_year = 1900
    gender = None
    education = {}
    
    def __init__(self, name, patron, surname, birth_year, gender, education):
        self.fio = Fio(name, patron, surname)
        try:
            self.birth_year = int(birth_year)
            if self.birth_year < 1900 or self.birth_year>2030:
                raise ValueError
        except ValueError:
            raise Exception('Birth year incorrect : ', birth_year)
        
        self.gender = Gender(gender)
        if education != None and education != {}:
            self.education.update(education)

    
    def __repr__(self):
        return '{People:('+str(self.fio)+', '+str(self.birth_year)+', '+\
            str(self.gender)+ ', '+ str(self.education)+')}'

if __name__=="__main__":
    me = People("Виктор","Андреевич","Ильясов",1971,"му", {"4 класса и два коридора":1990})
    print(me)
