# https://youtu.be/OUo0DNRqznc
# Гадалка

from random import randint
from brain0 import was_asked, old_answer

memory = []
prompt = "Что Вас интересует?"
answers = "да","нет"
question = "1"

while True:
    print(prompt, end = ' ')
    question = input()
    if question == "":
        print('Приходите ещё!')
        break
    if was_asked(memory, question):
        print('Я уже отвечала! Мой ответ -',old_answer())
    else:
        answer = answers[randint(0, len(answers) - 1)]
        print(answer)
        memory.append((question, answer))
