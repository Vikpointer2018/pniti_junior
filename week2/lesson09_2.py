# https://www.youtube.com/watch?v=nC8tH5nWvA0
# 2) Сделать так, чтобы она помнила отвеченные вопросы , 
# и не отвечала на них

from random import randint

def get_answer():
    return "да" if randint(0,1)==0 else "нет"

questions = set()
question = "1"
while True:
    question = input("Спрашивай! (если хватит - промолчи)")
    if len(question) == 0:
        print("Пока!")
        break
    if question in questions:
        print("Дважды на один вопрос не отвечаю!")
        continue
    questions.add(question)
    print(get_answer())
