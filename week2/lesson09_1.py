# https://www.youtube.com/watch?v=nC8tH5nWvA0
# 1) Написать программу, которая на каждый вопрос отвечает 
# "да" или "нет" случайным  образом

from random import randint

def get_answer():
    return "да" if randint(0,1)==0 else "нет"


question = "1"
while True:
    question = input("Спрашивай! (если хватит - промолчи)")
    if len(question) == 0:
        print("Пока!")
        break
    print(get_answer())
