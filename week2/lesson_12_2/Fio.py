# -*- coding: utf-8 -*-
class Fio:
    name = ""
    patron = ""
    surname = ""
    
    def __init__(self, name, patron, surname):
        if name == None or surname == None: 
            raise Exception('Fio is None')
            return
        if name == "" or surname == "":
            raise Exception('Fio is empty')
            return
        self.name, self.surname = name, surname
        self.patron = "" if patron == None else patron
        
    def __repr__(self):
        return '{Fio:("'+self.name+'","'+self.patron+'","'+self.surname+'")}'
    
    
if __name__ == "__main__":
    print(Fio("Елена","Анатольевна","Макеева"))
    