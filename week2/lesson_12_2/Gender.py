class Gender:
    __gen = "муж"
    
    def __init__(self, gen: str):
        if gen.lower().startswith("ж") or gen.lower().startswith("f"):
            self.__gen = "жен"
    
    
    def __repr__(self):
        return '{Gender:("'+self.__gen+'")}'
        
    def get_gen(self):
        return self.__gen
    
    
if __name__ == "__main__":
    g = Gender("му")
    if g.get_gen() == "муж":
        print("gender test1 Ok")
    else:
        print("gender test1 Fail")
        
    g1 = Gender("ggg")
    if g1.get_gen() == "муж":
        print("gender test2 Ok")
    else:
        print("gender test2 Fail")
        
    g2 = Gender("female")
    if g2.get_gen() == "жен":
        print("gender test3 Ok")
    else:
        print("gender test3 Fail")
        
    print(g)