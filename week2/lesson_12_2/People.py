# https://youtu.be/gyc6CPgxdG4
# ООП
# 2) Добавить несколько методов
from Fio import Fio
from Gender import Gender
from datetime import date

class People:
    fio = None # ФИО - выполнено в отдельном классе для удобства обработки
    birth_year = 1900 # Год рождения
    gender = None # Пол, принимает значения "муж" или "жен".
    # Проверка валидности ввода вынесена в отдельный класс
    __education = {} # Образование, по жизни добавляется, и изменяется
    # содержит названия учебных заведений и годы окончания
    
    def __init__(self, name, patron, surname, birth_year, gender, education):
        self.fio = Fio(name, patron, surname)
        try:
            self.birth_year = int(birth_year)
            if self.birth_year < 1900 or self.birth_year>2030:
                raise ValueError
        except ValueError:
            raise Exception('Год рождения указан некорректно : ', birth_year)
        
        self.gender = Gender(gender)
        if education != None and education != {}:
            self.__education.update(education)

    
    def __repr__(self):
        return '{People:('+str(self.fio)+', '+str(self.birth_year)+', '+\
            str(self.gender)+ ', '+ str(self.__education)+')}'


    def get_age(self): # Возвращает возраст, вычисляя его по текущей дате
        current_year = date.today().year
        age = current_year - self.birth_year
        return age
    
    
    def add_education(self, edu_name, end_year): # Добавление образования
        if edu_name == None or edu_name == "":
            return
        try:
            end_year = int(end_year)
            if end_year > self.birth_year:
                self.__education.update({edu_name:end_year})
        except: # Если название ВУЗа отсутствует, либо год невалидный
        # добавление не производим
            return
        
        
    def get_education(self): # Просмотр образования
        return self.__education
    


if __name__=="__main__":
    me = People("Виктор","Андреевич","Ильясов",1973,"му", {"4 класса и два коридора":1990})
    print(me)
    print("Возраст %s лет" % me.get_age())
    print(me.get_education())
    me.add_education("Политех", 1995)
    print(me.get_education())
    
