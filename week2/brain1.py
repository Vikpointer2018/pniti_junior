# https://www.youtube.com/watch?v=OUo0DNRqznc
# Brain
from random import randint
from re import sub
from itertools import product
from pprint import pprint

class Brain:
    ANSWERS = "да", "нет"
    MIN_WORD_SIMILARITY = 0.8
    MIN_QUESTION_SIMILARITY = 0.5
    memory = dict()
    
    def __fuzzy_key_search(self, words):
        answer = ''
        #print('fuzzy1', words)
        for key in self.memory:# ПОЧЕМУ БЕРЁТСЯ ТОЛЬКО ОДИН КЛЮЧ?!!!
            #print('fuzzy2', key, len(self.memory))
            res = []
            for w1, w2 in product(key, words):
                w = self.__compare(w1, w2)
                #print('compare ',w1,w2,w)
                if w > self.MIN_WORD_SIMILARITY:
                    res += [ (w , w1, w2) ]
                    print (" ", w , w1, w2)
            if len(res)>0 and sum(x[0] for x in res) / len(res) > self.MIN_QUESTION_SIMILARITY:
                answer = self.memory[key]
            # pprint(res)
        assert answer != ''
        return answer
    
    def __compare(self, s1, s2): # нечеткое сравнение
        count = 0
        for ngram in ( s1[i:i+3] for i in range(len(s1)) ):
            count += s2.count(ngram)
        return count / max(len(s1), len(s2))
    
    def think(self, question):
        print(question)
        words = tuple(sub(r'[^A-Za-zА-Яа-яЁё0-9]',' ', question).split())
        try:
            answer = self.memory[words]
            return "Я уже отвечала! Мой ответ - %s." % self.memory[words]
        except KeyError:
            try:
                return "Я уже кажется отвечала! Мой ответ - %s." %\
                    self.__fuzzy_key_search(words)
            except (AssertionError, ZeroDivisionError):
                answer = self.ANSWERS[randint(0, len(self.ANSWERS) - 1)]
                self.memory.update({words:answer})
                print(words, ' added to memory')
        return answer
    


if __name__ == "__main__":
    brain = Brain()
    print(brain.think('Ты умеешь готовить?'))
    print(brain.think('Ты умеешь готовить?'))
    print(brain.think('Ты умеешь говорить?'))
    print(brain.think('Маша ела кашу'))
    print(brain.think('Мама мыла раму'))
    print(brain.think('Маша ела кашку'))
