# https://www.youtube.com/watch?v=nC8tH5nWvA0
# 3) Сделать так, чтобы она помнила отвеченные вопросы, 
#  даже переформулированные

from random import randint
from re import sub

answers = ("да", "нет")
EMPTY_WORDS = {"будет","был","была","придет",\
    "станет","получу","решится"}

def get_answer():# Возвращает случайную строку "да" или "нет"
    return answers[randint(0, len(answers) - 1)]


def compare(s1, s2):# Сравнивает неточно две строки, возвращает [0.0-1.0+]
    if len(s1)==0 or len(s2)==0:
        return 0
    ngrams = [ s1[i:i+3].lower() for i in range(len(s1)) ]
    count = 0
    for ngram in ngrams:
        count += s2.lower().count(ngram)
    return count / max(len(s1), len(s2))


def check_word(word):# Проверяет похожесть слова на уже имеющиеся в словаре
    # возвращает маскимальную величину коэффициента сравнения
    if len(word)==0:
        return 0
    max_comp = 0
    curr_comp = 0
    for w in questions:
        curr_comp = compare(w, word)
        if curr_comp > max_comp:
            max_comp = curr_comp
    if max_comp == 1:
        return 1
    questions.add(word)# Если слово не пустое, и не совпадает с имеющимися
    # то добавляем его в множество
    return max_comp


questions = set()
question = "1"
while True:
    question = sub(\
    '[^A-Za-zА-Яа-яЁё]',' ',(input("Спрашивай! (если хватит - промолчи)")))
    question = sub('\s+',' ',question)
    if len(question) < 3:
        print("Пока!")
        break
    quest_words = set(question.split())
    len_quest = len(quest_words)
    quest_words = quest_words - EMPTY_WORDS
    count = 0
    for word in quest_words:
        count += check_word(word)

    if count/len_quest > 0.7:# если средняя похожесть вопроса > 0.75
        print("Дважды на один вопрос не отвечаю!")
        continue
    print(get_answer())
