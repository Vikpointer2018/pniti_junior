# взять текст (например Войну и Мир) и посчитать в нем частоту разных н-грамм
# составить словарь веса н-грамм для сравнения строк.
# Чем реже встречается н-грамма в языке (тексте) тем более она значима.
# самые значимые имеют вес 1, остальные - меньше
# сравнить "адекватность" способов

import re

text = ''
ngram_power = 3
data_file_name = './war.txt'

try:
    with open(data_file_name, 'r', encoding='utf-8') as file:
        text = file.read()
except FileNotFoundError:
    print('File '+data_file_name+' not found')
    exit

print('Text loaded successfull. Length = ', len(text))

text2 = re.sub('[^А-Яа-яЁё]',' ',text)
text3 = re.sub('\s+',' ',text2).lower()
words = text3.split()

ngrams_dict = {}
for w in words:
    ngrams = [ w[i:i+ngram_power] for i in range(len(w)) ]
    for ng in ngrams:
        if ng in ngrams_dict:
            ngrams_dict[ng] = ngrams_dict[ng]+1
        else:
            ngrams_dict[ng] = 1

for ng in ngrams_dict:
    ngrams_dict[ng] = round(1 / ngrams_dict[ng] , 5)
    if ngrams_dict[ng] < 0.00001:
        ngrams_dict[ng] = 0

outstr = str(sorted(ngrams_dict.items(), key=lambda x: -x[1]))
outstr = re.sub('\s*\)\s*,',';\n',outstr)
outstr = re.sub('[\(\)\[\]]','', outstr)
outstr = re.sub(',',';',outstr)

file_name_result = 'ngrams_dict_'+str(ngram_power)+'.csv'
with open(file_name_result, 'w') as f:
    f.write(outstr)

print('Result file ' + file_name_result + ' saved successfully')

"""
Расчёт выполнялся для ngram степени 2,3 и 4
Получены следующие результаты:
-------+---------+----------+--------------
Степень! вес=1.0 ! вес<=0.1 ! всего н-грам
-------+---------+----------+--------------
   2   !     38  !     101  !       785
   3   !    733  !    2395  !      7102
   4   !   5613  !   16761  !     29275
-------+---------+----------+--------------
Очевидно, что словарь 4-символьных n-грам избыточен и ресурсоёмок
2-символьные n-граммы наоборот, недостаточно информативны.
Оптимальная степень - 3 символа.
"""

