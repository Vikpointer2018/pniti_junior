# https://www.youtube.com/watch?v=vMJfCxBsVoI
# 3) Сделать list такой, чтобы его элементы были уникальными

class List(set):

    def __init__(self,a_list=set()):
        if a_list==None:
            a_list = set()
        super().__init__(a_list)
    
    def __add__(self,a_list=[]):
        return List(super().union(set(a_list)))
    
    def append(self,x=None):
        if x == None:
            return self
        return List(super().add(x))


if __name__=="__main__":
    print(List())
    print(List([9,0]))
    print(list(range(10,2,-1)))
    lst = List(range(10,2,-1))
    print(lst)
    lst = lst + {1,2,3,4,5}
    print(lst)
    lst.append(14)
    print(lst)