# https://www.youtube.com/watch?v=vMJfCxBsVoI
# 2) Сделать list такой, чтобы его длина не могла превысить 10

class MyList(list):

    def __init__(self,a_list=[]):
        if a_list == None:
            a_list = []
        if len(a_list)>10:
            raise ValueError("Len MyList is more than 10")
        super().__init__(a_list)

    def append(self, x):
        if super().__len__() > 9:
            raise ValueError("Len MyList is more than 10")
        super().append(x)
    

if __name__=="__main__":
    print(MyList())
    print(MyList(None))
    print(MyList([9,0]))
    print(MyList(range(10)))
    m_list = MyList()
    
    for i in range(5):
        m_list.append(i)
    print(m_list)
    for j in range(7):
        m_list.append(j)
        
        print(m_list)
    
    # print(MyList(range(11)))
    

        
