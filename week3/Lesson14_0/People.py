# https://www.youtube.com/watch?v=vMJfCxBsVoI
# Доработка класса человек
from Person import Person

class People(dict):
    def __compare__(self, s1, s2):
        count = 0
        for ngram in (s1[i:i+3].lower() for i in range(len(s1))):
            count += s2.lower().count(ngram)
        return count / max(len(s1), len(s2)) > 0.5
    
    def __getitem__(self, k1):# ключ tuple(имя, отчество, фамилия, год)
        res = People()
        for k2, v in self.items():
            comp = True
            if (k1[0]=='' or k1[0]==None) and (k1[2]=='' or k1[2]==None):
                continue
            if k1[0] != '' and k1[0] != None:
                comp = comp and self.__compare__(k1[0], k2[0])
                
            if k1[1] != '' and k1[1] != None:
                comp = comp and self.__compare__(k1[1], k2[1])
            
            if k1[2] != '' and k1[2] != None:
                comp = comp and self.__compare__(k1[2], k2[2])
            
            if k1[3] != '' and k1[3] != None:
                try:
                    b_year = int(k1[3])
                    comp = comp and (abs(b_year-k2[3])<2)
                except ValueError:
                    continue
            
            if comp:
                res.update(v)
        return res
    
    def update(self, obj):
        super().update({obj.key:obj})
        
    def __repr__(self):
        return "People(%s)" % super().__repr__()
    

if __name__=="__main__":
    nata = Person('Наталья', 'Александровна', 'Ростова', 1978, "ж", \
                  "Уральская", 113, 2, ['Школа №1', 1995])
    alex = Person('Алексей', 'Михалыч', 'Кудряшов', 1968, "м", \
                  "Васильева", 27, 5, ['ПВВКИУ', 1990])  
    nata2 = Person('Наталья', 'Васильевна', 'Лунёва', 1973, "ж", \
                  "Калинина", 36, 13, ['ПГУ', 1995])
    people = People()
    people.update(nata)
    people.update(alex)
    people.update(nata2)
    
    print(people)
    print("--- Поиск людей по неточным данным ---")
    print("'алекс', 'михайлыч', 'кудряшЕв', 1967 :\n",people[('алекс', 'михайлыч', 'кудряшЕв', 1967)])
    print("'александр', '', '', '' :\n",people[('александр', '', '', '')])
    print("'', '', 'ростова', '' :\n",people[('', '', 'ростова', '')])
    print("'наталя', '', '', '' :\n",people[('наталя', '', '', '')])
    
            