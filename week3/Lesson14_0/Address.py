class Address:
    street = ''
    house = ''
    apart = ''
    
    def __init__(self, street, house, apart):
        self.street = street if street != None else ''
        self.house = house if house != None else ''
        self.apart = apart if apart != None else ''
    
    def __repr__(self):
        return 'Address:("%s", %s-%s)' %\
            (self.street,self.house,self.apart)
    
if __name__=="__main__"       :
    print(Address("Ленина", 15, 28))
    print(Address("Комсомоьский пр.", "29А", "415"))
