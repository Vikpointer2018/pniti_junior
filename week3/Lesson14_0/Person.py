# https://www.youtube.com/watch?v=vMJfCxBsVoI
# Доработка класса человек
from datetime import date

from Fio import Fio
from Gender import Gender
from Address import Address

class Person:
    fio = None
    b_year = 1900
    gender = None
    address = None
    education = None

    def __init__(self, a_name, patron, surname, b_year, gender,\
                 street, house, apart, educat):
        self.fio = Fio(a_name, patron, surname)
        try:
            b_year = int(b_year)
            if b_year > 1900 and b_year < 2030:
                self.b_year = b_year
        except:
            pass
        self.gender = Gender(gender)
        self.address = Address(street, house, apart)
        self.education = [list(educat)] if educat!=None else []
        
        self.key = (a_name, patron, surname, b_year)

    def __repr__(self):
        return "Person:(%s, %s, %s, %s, %s)" % (str(self.fio), str(self.b_year),\
               str(self.gender),str(self.address),str(self.education))
            
    def add_education(self, new_educat):
        self.education += [list(new_educat)]
        
    def get_age(self): # Возвращает возраст, вычисляя его по текущей дате
        current_year = date.today().year
        age = current_year - self.b_year
        return age


if __name__ == "__main__":
    fio = Fio("Олег", "Владимирович", "Тараненко")
    gender = Gender("м")
    address = Address("Большевистская", 165, 47)
    education = {"ВШЭ":1993}
    oleg = Person("Олег", "Владимирович", "Тараненко",1970,"m",\
                  "Большевистская", 165, 47, ["ВШЭ",1993])
    print(oleg)
    oleg.add_education(["Политех", 2000])
    print(oleg)
    print(oleg.get_age())
    
