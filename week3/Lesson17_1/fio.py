class Fio:
    """
    Класс Фамилия, Имя, Отчество, пол.
    Умеет сравниваться нечётко
    
    """
    def __init__(self, a_name, patron, surname, gender:str="m"):
        a_name = "" if a_name == None else a_name
        surname = "" if surname == None else surname
        if a_name == "" and surname == "":
            raise Exception('Fio is empty')
            return
        self._name = a_name
        self.surname = surname
        self.patron = "" if patron == None else patron
        self.gender = "" if gender == None or gender == '' \
            else "жен" if gender.lower().startswith("ж")\
                or gender.lower().startswith("f") else "муж"
        
    def __repr__(self):
        return 'Fio:(%s %s %s %s)' %\
            (self._name, self.patron, self.surname, self.gender)
    
    def fuzzy_compare(self, other):
        """
        Нечёткое сравнение ФИО.
        пол если указан, должен совпадать полностью,
        отчество опционально, фамилия и/или имя должны быть указаны
        """
        if other == None:
            return False
        try:
            if other.gender != '' and self.gender != other.gender:
                return False
            if other._name == '' and other.surname == '':
                return False
            if other._name != '' and not self.__compare__(self._name, other._name):
                return False
            if other.patron != '' and not self.__compare__(self.patron, other.patron):
                return False
            if other.surname != '' and not self.__compare__(self.surname, other.surname):
                return False
        except:
            return False
        return True
    
    def __eq__(self, other):
        if other == None:
            return False
        if self.__hash__() != other.__hash__():
            return False
        try:
            if self.gender != other.gender:
                return False
            if self._name != other._name:
                return False
            if self.patron != other.patron:
                return False
            if self.surname != other.surname:
                return False
        except:
            return False
        return True
    
    def __compare__(self, s1, s2):
        count = 0
        if s1<s2:
            s1, s2 = s2, s1
        for ngram in (s1[i:i+3].lower() for i in range(len(s1))):
            count += s2.lower().count(ngram)
        comp = count / max(len(s1), len(s2))
        return comp > 0.6
    
    def __hash__(self):
        hash = self._name.__hash__() + self.patron.__hash__() \
            + self.surname.__hash__() + self.gender.__hash__()
        return hash
    
if __name__ == "__main__":
    helen = Fio("Елена","Анатольевна","Макеева", "female")
    helen2 = Fio("лена","натолевна","макева", "ЖЕН")
    milen = Fio("милен","Васильевна","Макеева", "ж")
    helen3 = Fio("Елена","Анатольевна","Макеева", "female")
    
    alex = Fio('Алексей', 'Михалыч', 'Кудряшов', "м")
    alex2 = Fio('алекс', 'михайлыч', 'кудряшЕв', "м")
    print(helen, " hash=", helen.__hash__())
    print(helen2, " hash=", helen2.__hash__())
    print(milen, " hash=", milen.__hash__())
    print('=====TEST FUZZY_COMPARE======')
    comp1 = helen.fuzzy_compare(helen2)
    print(comp1, "Test OK" if comp1 else "Test Failure!")
    
    comp2 = helen.fuzzy_compare(milen)
    print(comp2, "Test OK" if not comp2 else "Test Failure!")
    
    comp5 = alex.fuzzy_compare(alex2)
    print(comp5, "Test OK" if comp5 else "Test Failure!")


    print('=====TEST EQUAL====')
    
    comp3 = helen == helen3
    comp4 = helen == helen2
    print(comp3, "Test OK" if comp3 else "Test Failure!")
    print(comp4, "Test OK" if not comp4 else "Test Failure!")

    