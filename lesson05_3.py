# https://youtu.be/4m_P-3xW0lA
# 3) Сделать нечёткое сравнение элементов

def compare_str(str1, str2):
    ngrams = [ str1[i:i+3] for i in range(len(str1)) ]
    count = 0
    for ngram in ngrams:
        count += str2.count(ngram)
    return count/max(len(str1), len(str2))

print(compare_str('синхрофазотрон','синхронизатор'))
print(compare_str('гидроэлектростанция','электровоз'))
print(compare_str('камасутра','ранарама'))
