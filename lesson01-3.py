# https://youtu.be/bByq3BVqRZw
# Типы данных
# 1) Описать себя, используя список
myself_list = [
    "Виктор", "Мальцев", 1973, "муж", "3 класса церковно-приходской"
    ]
print(myself_list)

# 2) Описать себя, используя словарь
myself_dict = {
    "name": "Виктор",
    "lastName": "Мальцев",
    "birthYear": 1973,
    "gender": "муж",
    "education": "3 класса церковно-приходской"
    }
print(myself_dict)

# 3) Объяснить, какое описание удобнее с точки зрения обработки информации
#    и почему
"""
Разнородные данные удобнее описывать словарями, так как явно именуются поля,
однородные данные имеет смысл описывать списками в целях экономии памяти
"""

